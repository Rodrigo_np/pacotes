<?php

namespace App\Repositories;

use App\Contracts\FormInterface as RepositoryInterface;
// use App\Repositories\CurlRequestRepository as CurlRequest;
use App\Http\Controllers\CurlRequestController as CurlRequest;

class FormRepository implements RepositoryInterface {

    /**
     * Create a new repository instance.
     *
     * @param  CurlRequest  $curl
     * @return void
     */
    public function __construct(CurlRequest $curl) {
        $this->curl = $curl;
    }

    public function getForms() {
        $url = "forms";
        $response = $this->curl->curlRequest($url, null);

        return $response;
    }

}