<nav class="navbar navbar-light bg-white">
    <button class="btn" onclick="window.history.back()"><i class="fas fa-undo"></i> Voltar</button>
    
    <a href="{{ route('logout') }}" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Sair <i class="fas fa-sign-out-alt"></i></a>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
</nav>