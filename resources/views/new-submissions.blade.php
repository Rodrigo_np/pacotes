@extends('index')

@section('title', 'Submissão')

@section('content')

    <h3 class="text-center">Novas Submissões</h3>

    <table id="new-submissions-table" class="table table-hover table-bordered data-table">
        <thead>
            <tr>
                <th>Formulário</th>
                <th>Submissão</th>
                <th>Enviado em</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($new_submissions as $new)
                <tr>
                    <td>
                        <p><a href="{{ route('submissions', $new->form_id) }}">{{ $new->form_name }}</a></p>
                    </td>
                    <td>
                        <p><a href="{{ route('submission', $new->submission_guid) }}">{{ $new->submission_guid }}</a></p>
                    </td>
                    <td>
                        <p>{{ date('d/m/Y H:i', strtotime($new->created_at)) }}</p>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection