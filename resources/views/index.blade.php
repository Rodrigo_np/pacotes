<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
</head>
<body>

    {{-- Main container --}}
    <div id="main-container" class="container-fluid">
        <div class="row">
            <aside class="col-2">
                <div class="logo text-center mt-5 border-bottom">
                    @include('layouts.svgs.box')
                    <p><b>PACOTES</b></p>
                </div>
                {{-- End of logo div --}}

                <ul class="mt-5">
                    <li><a href="{{ route('forms') }}">Home</a></li>
                    <li><a href="{{ route('new-submissions') }}">Novas Submissões</a></li>
                </ul>
            </aside>
        
            <main class="col">
                @include('layouts.parts.navbar')
                <div class="content mt-5">
                    @yield('content')
                </div>
                {{-- End of content div --}}
            </main>
        </div>
        {{-- End of row --}}
    </div>
    {{-- End of main container --}}

    <script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
</body>
</html>