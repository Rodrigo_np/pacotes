@extends('index')

@section('title', 'Submissão')

@section('content')

<?php
    $username = "neferson@kasulopay.com.br";
    $password = "Kasulo@pay";
    $submission = $submissionXml->Submissions->Submission; 
    $date = str_replace('.','/', (string) $submission->Date);
    $date = new DateTime($date.' +00');
    $date->setTimezone(new DateTimeZone('America/Sao_Paulo'));
?>

    <div class="info mb-5">
        <h3 class="mb-5 text-center">Informações da submissão "{{ $submission->ResponseID }}"</h3>
        <h5>Nome: {{ $submission->Sections->Section->Name }}</h5>
        <h5>Submissão de No. {{ $submission->SubmissionNumber }}</h5>
        <h5>Enviado em: {{ date_format($date, 'd/m/Y H:i') }} </h5>
        <h5>Enviado por: {{ $submission->FirstName. ' ' .$submission->LastName }}</h5>
    </div>
    {{-- End of info div --}}

    <h3 class="text-center mb-4">Dados enviados</h3>

    <table id="submission-table" class="table table-hover table-bordered">
            <thead>
                <tr>
                    <th>Campo</th>
                    <th>Valor</th>
                </tr>
            </thead>
    
            <tbody>
                @foreach ($submissionXml->Submissions->Submission->Sections->Section->Screens->Screen->Responses->Response as $submission)
                    <tr>
                        <td>
                            <p>{{ $submission->Label }}</p>
                        </td>
                        <td>
                            @if (!($submission->Label == "Comprovante"))
                                <p>{{ $submission->Value }}</p>
                            @else 
                                <div id="accordion">
                                    <div class="card bg-transparent border-0 p-0">
                                        <div class="card-header bg-transparent border-0 p-0" id="headingImage">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link p-0" data-toggle="collapse" data-target="#collapseImage" aria-expanded="true" aria-controls="collapseImage">
                                                    {{ $submission->Value }}
                                                </button>
                                            </h5>
                                        </div>
                                        {{-- End of card header --}}
                                    
                                        <div id="collapseImage" class="collapse" aria-labelledby="headingImage" data-parent="#accordion">
                                            <div class="card-body">
                                                <?php
                                                    $image_url = "https://www.gocanvas.com/apiv2/images.xml?image_id=$submission->Value&username=$username&password=$password"
                                                ?>
                                                <img src="{{ $image_url }}" alt="Comprovante {{ $submission->Value }}">
                                            </div>
                                        </div>
                                        {{-- End of collapse --}}
                                    </div>
                                    {{-- End of card div --}}
                                </div>
                                {{-- End of accordion --}}
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
    </table>
@endsection