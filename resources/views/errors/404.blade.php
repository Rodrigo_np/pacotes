@extends('index')

@section('title', 'ERRO 404')

@section('content')

    <style scoped>
        #main-container aside {
            display: none !important;
        }

        #main-container main {
            background-color: #fff;
        }
        
        #main-container main .content {
            box-shadow: 0 0 0;
        }

        #main-container main .navbar {
            display: none;
        }
    </style>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <p class="display-2">Erro 404</p>
                <p class="display-4">A página requisitada não foi encontrada.</p>
                <p class="text-center">
                    <button class="btn btn-primary mt-5" onclick="window.history.back()">Leve-me de volta!</button>
                </p>
            </div>
            {{-- End of col --}}
        </div>
        {{-- End of row --}}
    </div>
    {{-- End of container --}}

@endsection