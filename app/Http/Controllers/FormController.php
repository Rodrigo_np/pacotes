<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\FormRepository as Form;

class FormController extends Controller
{

    /**
     * Create a new repository instance.
     *
     * @param Form  $form
     * @return void
     */
    public function __construct(Form $form) {
        $this->form = $form;
    }

    public function index() {
        $forms = $this->form->getForms();

        $formsXml = simplexml_load_string($forms);

        return view('forms', compact('formsXml'));
    }
}
