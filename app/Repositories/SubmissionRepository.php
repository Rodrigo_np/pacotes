<?php

namespace App\Repositories;

use App\Contracts\SubmissionInterface as RepositoryInterface;
// use App\Repositories\CurlRequestRepository as CurlRequest;
use App\Http\Controllers\CurlRequestController as CurlRequest;

class SubmissionRepository implements RepositoryInterface {
    
    /**
     * Create a new repository instance.
     *
     * @param  CurlRequest  $curl
     * @return void
     */
    public function __construct(CurlRequest $curl) {
        $this->curl = $curl;
    }

    // Get all submissions of a form (this $id is the form id)
    public function getSubmissions($id) {
        $url = "submissions";
        $response = $this->curl->curlRequest($url, $id);

        return $response;
    }

    public function getSubmissionbyId($id) {
        $url = "submission_by_id";
        $response = $this->curl->curlRequest($url, $id);

        return $response;
    }

}