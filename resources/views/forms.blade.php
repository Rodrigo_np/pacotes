@extends('index')

@section('title', 'Pacotes')

@section('content')

    <h3 class="text-center mb-5">Formulários</h3>

    <table id="forms-table" class="table table-hover table-bordered data-table">
        <thead>
            <tr>
                <th>Nome do formulário</th>
            </tr>
        </thead>

        <tbody>
            @foreach ($formsXml->Forms->Form as $form)
                <tr>
                    <td>
                        <a href="{{ route('submissions', $form->attributes()->Id) }}">{{ $form->Name }}</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection