<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class NotificationController extends Controller
{
    public function index() {
        $new_submissions = DB::table('notifications')->get();

        return view('new-submissions', compact('new_submissions'));
    }

    public function notify(Request $request) {
        // Get the request body
        $content = $request->getContent();

        // Converting xml into object
        $xml = simplexml_load_string($content);
        // Converting object into json
        $json = json_encode($xml);
        $json = json_decode($json, true);

        $current_time = Carbon::now()->toDateTimeString();

        // Making a raw insert with the GoCanvas Webhook posted data
        $insert = DB::table('notifications')->insert([
            'form_id' => $json['form']['id'],
            'form_name' => $json['form']['name'],
            'submission_guid' => $json['submission']['guid'],
            'created_at' => $current_time,
            'updated_at' => $current_time,
        ]);

        return $insert ? 'true' : 'false';
    }
}
