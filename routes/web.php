<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Returning login page as first page
Route::get('/', function () {
    return redirect('login');
});

// Only authenticated users can access these routes
Route::group(['middleware' => 'auth'], function() {
    Route::get('formularios', 'FormController@index')->name('forms');
    Route::get('submissoes/{id}', 'SubmissionController@index')->name('submissions');
    Route::get('submissao/{id}', 'SubmissionController@getSubmissionbyId')->name('submission');
    Route::get('notification', function() {
        return redirect('novas-submissoes');
    });
    Route::get('novas-submissoes', 'NotificationController@index')->name('new-submissions');
});

// GoCanvas Webhook route
Route::post('notification', 'NotificationController@notify')->name('notification');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes redirection
Route::get('register', function() {
    return redirect('login');
})->name('register');

Route::post('register', function() {
    return redirect('login');
});

// Password Reset Routes redirection
Route::get('password/reset', function(){
    return redirect('login');
})->name('password.request');

Route::post('password/email', function(){
    return redirect('login');
})->name('password.email');

Route::get('password/reset/{token}', function(){
    return redirect('login');
})->name('password.reset');

Route::post('password/reset', function(){
    return redirect('login');
});
