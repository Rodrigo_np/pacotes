@extends('index')

@section('title', 'Submissões')

@section('content')

    @if (!($submissionsXml->Submissions->Submission))
        <p class="text-center"><i class="far fa-frown fa-3x"></i></p>
        <p class="text-center" style="font-size: 1.5rem;">Não há submissões para este formulário ainda.</p>
    @else

        <h3 class="text-center mb-5">
            Submissões do formulário 
            <span class="form-name">"{{ $submissionsXml->Submissions->Submission->Form->Name }}"</span>
        </h3>

        <table id="submissions-table" class="table table-hover table-bordered data-table">
            <thead>
                <tr>
                    <th>Nº da submissão</th>
                    <th>ID da Submissão</th>
                    <th>Data do envio</th>
                    <th>Enviado por</th>
                </tr>
            </thead>
    
            <tbody>
                @foreach ($submissionsXml->Submissions->Submission as $submission)
                    <tr>
                        <td>
                            <p>{{ $submission->SubmissionNumber }}</p>
                        </td>
                        <td>
                            <a href="{{ route('submission', $submission->attributes()->Id) }}">{{ $submission->ResponseID }}</a>
                        </td>
                        <td>
                            <p>
                                <?php 
                                    $date = str_replace('.','/', (string) $submission->Date);
                                    $date = new DateTime($date.' +00');
                                    $date->setTimezone(new DateTimeZone('America/Sao_Paulo'));
                                ?>
                                {{ date_format($date, 'd/m/Y H:i') }}
                            </p>
                        </td>
                        <td>
                            <p>{{ $submission->FirstName. ' ' .$submission->LastName }}</p>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
@endsection