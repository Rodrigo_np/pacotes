<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SubmissionRepository as Submissions;

class SubmissionController extends Controller
{
    /**
     * Create a new repository instance.
     *
     * @param Submissions $submissions
     * @return void
     */
    public function __construct(Submissions $submissions) {
        $this->submissions = $submissions;
    }

    // Get all submissions of a form
    public function index($id) {
        $submissions = $this->submissions->getSubmissions($id); // This $id is the form id
        $submissionsXml = simplexml_load_string($submissions);    

        return view('submissions', compact('submissionsXml'));
    }

    public function getSubmissionById($id) {
        $submission = $this->submissions->getSubmissionById($id); // This $id is the submission id
        $submissionXml = simplexml_load_string($submission);

        return view('submission', compact('submissionXml'));
    }
}
