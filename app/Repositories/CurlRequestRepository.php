<?php

namespace App\Repositories;

use App\Contracts\CurlRequestInterface as RepositoryInterface;

class CurlRequestRepository implements RepositoryInterface {

    // Api auth parameters
    private $username = "neferson@kasulopay.com.br";
    private $password = "Kasulo@pay";

    public function curlRequest($url, $id = null) {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,   // return web page
            CURLOPT_HEADER         => false,  // don't return headers
            CURLOPT_FOLLOWLOCATION => true,   // follow redirects
            CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
            CURLOPT_ENCODING       => "",     // handle compressed
            CURLOPT_USERAGENT      => "",     // name of client
            CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
            CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
            CURLOPT_TIMEOUT        => 120,    // time-out on response
        );

        // Depending on the request type, the request url changes
        if($url == "forms") {
            $url = "https://www.gocanvas.com/apiv2/forms.xml?username=".$this->username."&password=".$this->password;
        }

        if($url == "submissions") {
            $url = "https://www.gocanvas.com/apiv2/submissions.xml?username=".$this->username."&password=".$this->password."&form_id=".$id;
        }

        if($url == "submission_by_id") {
            $url = "https://www.gocanvas.com/apiv2/submissions.xml?username=".$this->username."&password=".$this->password."&id=".$id;
        }

        $curl = curl_init($url);
        curl_setopt_array($curl, $options);

        $content = curl_exec($curl);

        curl_close($curl);

        return $content;
    }

}