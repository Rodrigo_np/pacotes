<?php

namespace App\Contracts;

interface SubmissionInterface {

    public function getSubmissions($id);
    
    public function getSubmissionById($id);
}