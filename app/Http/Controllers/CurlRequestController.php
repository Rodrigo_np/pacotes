<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CurlRequestRepository as CurlRequest;

class CurlRequestController extends Controller
{
    /**
     * Create a new repository instance.
     *
     * @param CurlRequest $curl
     * @return void
     */
    public function __construct(CurlRequest $curl) {
        $this->curl = $curl;
    }

    public function curlRequest($url, $id) {
        return $this->curl->curlRequest($url, $id);
    }
}
