<?php

namespace App\Contracts;

interface CurlRequestInterface {

    public function curlRequest($url);
}