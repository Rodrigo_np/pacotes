'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
	setTimeout(function(){
		return gulp.src('./resources/assets/sass/main.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest('./public/css'));
	});
	
});

gulp.task('default', function(){
		gulp.watch('./resources/assets/sass/main.scss',['sass']);
});